#include <stdio.h>
#include <stdlib.h>
#define N 10
typedef long long ll;
ll fl[100005];
ll Koll(ll num, ll count) {
	if (fl[num])
		return fl[num]+count;
	if (num == 1)
		return count;
	if (num % 2)
		return Koll(3 * num + 1, count + 1);
	else
		return Koll(num / 2, count + 1);
}

int main() {
	ll max1=0, ans=1;
	for (ll i = 2; i <= 1000000; i++) {
		ll buf = Koll(i, 0);
		if (buf > max1){
			ans = i;
			max1 = buf;
		}
		fl[i] = buf;
	}
	printf("%d", ans);
	return 0;
}
