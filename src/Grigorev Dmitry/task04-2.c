#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define N 50

char buf[N][N];
int main() {
	srand(time(0));
	FILE *fin;
	fin=fopen("input.txt","r");
	char c;
	int i=0,fl=1,k,l,j=1;
	while(fl){//считывание строки
		int mas[N];
		for (k=0;k<N;k++)
			mas[k]=0;
		while ((c=getc(fin))!='\n' && j<N){//считывание слова
			if (c==' '){
				buf[i][0]=j;//в нулевом элементе хранится длинна слова
				i++;
				j=1;
				continue;
			}
			if (c==EOF){
				fl=0;
				break;
			}
			buf[i][j++]=c;
		}
		//printf("%d",j);
		buf[i][0]=j;
		j=1;
		int check=0;
		while (check==0){//вывод
			check=1;
			for (k=0;k<=i;k++)
				if (mas[k]==0)
					check=0;
			int ind;
			ind=rand()%(i+1);
			if (mas[ind]==0){
				for (l=1;l<buf[ind][0];l++)
					printf("%c",buf[ind][l]);
				printf(" ");
				mas[ind]=1;
			}
			else
				continue;
		}
		i=0;
		printf("\n");
	}
	return 0;
}
